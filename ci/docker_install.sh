#!/bin/bash

# prepare the build environment, installs all prerequisites prior the actual testing is done.

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe
apt-get update -yqq
apt-get install git wget zip unzip -yqq
wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php'); unlink('installer.sig');"
# docker-php-ext-install mbstring mcrypt pdo_mysql curl json intl gd xml zip bz2 opcache

# Install & enable Xdebug for code coverage reports
pecl install xdebug
docker-php-ext-enable xdebug

# copy env file
cp .env.test .env

# install composer
php composer.phar install --ignore-platform-reqs --no-ansi --no-interaction --no-progress
